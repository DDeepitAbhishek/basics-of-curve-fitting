% Purpose 
% Minimize the error using the probability approach with the knowledge of
% prior distribution.

% Inputs
% x - vector of x values generated 
% y - vector of y values generated
% t - vector of target values generated.(y+noise)
% M - order of the polynomial
% beta - the variance of the distribution
%sigma - the standard deviation given
%npts - number of data points
%alpha - hyperparameter for regularizing the coefficients

% Outputs
% W - estimate for the coefficients of the polynomial
% Y - predicted new data values
%% Start your curve fitting program here
%load the data
npts = input('enter the number of data sets');
switch(npts)
    case 10
        load data1.mat
    case 50
        load data2.mat
    case 300
        load data3.mat
end

%Get the order of the polynomial as input
M = input('enter the value of order M: ');

%Create the empty vector X with the length equal to number of the data points
X = zeros(npts,1);

%Create the matrix X for putting the values of the input data x 
%No of columns is equal to order 
%each column has the input data raised to the power of column number
%this is done to store the polynomial in X for M order
for i = 1:M
    X = [X transpose(x.^i)];
end

X(:,1) = 1;%first column is raised to power zero

sigma = 0.3;

%the beta parameter derived for maximum likelihood
beta = 1/(sigma^2);
err = ones(size(x))*(1/sqrt(beta));

alpha = input('Enter the value of alpha: ');
%The coefficients of the polynomial is found as derived in the report.
%\ operator is used for the ease of computation
W = (transpose(X)*X +(alpha/beta)*(eye(M+1)))\(transpose(X)*transpose(t));

%The predicted data values are found out
Y = X*W;

% plot the x and y color the area around the line by err (here the std =1/sqrt(beta)) 
%The curve fit is depicted in the graph.
hold on
h = shadedErrorBar(x,Y,err,{'b-','color','b','LineWidth',2},0);
plot(x,t,'ro','MarkerSize',8,'LineWidth',1.5)
plot(x,Y,'b','MarkerSize',8,'LineWidth',1.5)
hold off
grid on;
set(gca,'FontWeight','bold','LineWidth',2)
xlabel('x')
ylabel('t')