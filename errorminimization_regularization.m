% Purpose 
% Minimize the error for the predicted values of the data
% With regularizing parameter lambda

% Inputs
% x - vector of x values generated 
% y - vector of y values generated
% t - vector of target values generated.(y+noise)
% M - order of the polynomial
% npts - number of data points
% lambda- regularizing parameter

% Outputs
% W - estimate for the coefficients of the polynomial
% Y - predicted new data values

%% Start your curve fitting program here
%load the data
npts = input('enter the number of data sets');
switch(npts)
    case 10
        load data1.mat
    case 50
        load data2.mat
    case 300
        load data3.mat
end

%Get the order of the polynomial as input
M = input('enter the value of order M:');

%Create the empty vector X with the length equal to number of the data points
X = zeros(npts,1);

%Create the matrix X for putting the values of the input data x 
%No of columns is equal to order 
%each column has the input data raised to the power of column number
%this is done to store the polynomial in X for M order
for i = 1:M
    X = [X transpose(x.^i)];
end

X(:,1) = 1; %first column is raised to power zero

%get the regularizing parameter.
%exp() function is used for ease for giving input lambda eg- ln(lambda)=-18
lambda = exp(input('Enter the value of ln (lambda): '));

%The coefficients of the polynomial is found as derived in the report.
%\ operator is used for the ease of computation
W = (transpose(X)*X + lambda*eye(M+1))\(transpose(X)*transpose(t));

%The predicted data values are found out
Y = X*W;

%The curve is depicted in the plot with the ground truth
hold on
plot(x,t,'ro','MarkerSize',8,'LineWidth',1.5)
plot(x,Y,'b','MarkerSize',8,'LineWidth',1.5)
plot(x,y,'g','MarkerSize',8,'LineWidth',1.5)
hold off
grid on;
set(gca,'FontWeight','bold','LineWidth',2)
xlabel('x')
ylabel('t')

